This is just quick & dirty PoC to see how TypeORM with complex relational mappings works with NestJS

1. Run `npm install`
2. Create new DB in Postgres and fill in `ormconfig.json` with correct data
3. `npm run start`
4. Open `team.controller.ts` and `developer.controller.ts` to figure out the format of the requests