import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { DeveloperService } from './developer.service';

@Controller('/developer')
export class DeveloperController {
    public constructor(
        private developerService: DeveloperService,
    ) {}

    @Post()
    public async createDeveloper(
        @Body() developer : { name: string, teamName: string },
    ) {
        return this.developerService.createDeveloper(developer);
    }

    @Get('/:name')
    public async findByName(@Param() { name } : { name: string }) {
        return this.developerService.findDeveloperByName(name);
    }
}
