import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Developer } from './entity/developer.entity';
import { Repository } from 'typeorm';
import { TeamService } from '../team/team.service';
import { Team } from '../team/entity/team.entity';

@Injectable()
export class DeveloperService {
    public constructor (
        @InjectRepository(Developer)
        private readonly developerRepository: Repository<Developer>,
        private teamService: TeamService,
    ) {}

    public async createDeveloper({ name, teamName } : { name: string, teamName: string }) {
        const [ team ] = await this.teamService.findTeamByName(teamName);
        return this.developerRepository.save({ name, team });
    }

    public async findDeveloperByName(name: string) {
        const params = {
            where: { name },
            relations: ['team'],
        }
        return await this.developerRepository.find(params)
    }
}
