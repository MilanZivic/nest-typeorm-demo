import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Team } from "src/domain/team/entity/team.entity";

@Entity()
export class Developer {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @ManyToOne(type => Team, team => team.developers)
    public team: Team;
}