import { Module } from '@nestjs/common';
import { TeamModule } from './team/team.module';
import { DeveloperModule } from './developer/developer.module';

@Module({
  imports: [TeamModule, DeveloperModule]
})
export class DomainModule {}
