import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Developer } from 'src/domain/developer/entity/developer.entity';

@Entity()
export class Team {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @OneToMany(type => Developer, developer => developer.team)
    public developers : Developer[];
};