import { Controller, Post, Body, Get, Param } from "@nestjs/common";
import { TeamService } from "./team.service";

@Controller('/team')
export class TeamController {
    public constructor(
        private readonly teamService: TeamService,
    ) {}

    @Post()
    public async createTeam(@Body() body: { name: string }) {
        return this.teamService.createTeam(body);
    }

    @Get('/:name')
    public async findByName(@Param() { name }: { name: string }) {
        return this.teamService.findTeamByName(name);
    }
}