import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from './entity/team.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TeamService {
    public constructor(
        @InjectRepository(Team)
        private readonly teamRepository: Repository<Team>,
    ){}

    public async createTeam(team: { name: string }) {
        return await this.teamRepository.save(team);
    }

    public async findTeamByName(name: string) {
        return await this.teamRepository.find({ where: { name }, relations: ['developers'] });
    }
}
